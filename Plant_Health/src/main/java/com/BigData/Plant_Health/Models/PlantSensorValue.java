package com.BigData.Plant_Health.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter@Setter
@AllArgsConstructor
public class PlantSensorValue {
    private double lumen;
    private double soilMoisture;
    private double tempature;
    private double plantHealth;
    private PlantType plantType;
    private double size;


}
