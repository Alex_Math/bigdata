package com.BigData.Plant_Health.Services;

import com.BigData.Plant_Health.Models.PlantSensorValue;
import com.BigData.Plant_Health.Models.PlantType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@Setter
@Getter
@Component
public class PlantSensorSimulator {
    //private int id;
    private int minLumen;
    private int maxLumen;

    private double minSoilMoisture;
    private double maxSoilMoisture;


    private double minTempature;
    private double maxTempature;

    private double minPlantHealth;
    private double maxPlantHealth;

    private double minSize;
    private double maxSize;

    public PlantSensorSimulator(){}

    public PlantSensorValue SimlulateSersorValue(LocalDateTime timeStamp){
        Random generator = new Random();

        double lumen =ThreadLocalRandom.current().nextDouble(minLumen, maxLumen);
        double soilMoisture = ThreadLocalRandom.current().nextDouble(minSoilMoisture, maxSoilMoisture);
        double tempature = ThreadLocalRandom.current().nextDouble(minTempature, maxTempature);
        double plantHealth =  ThreadLocalRandom.current().nextDouble(minPlantHealth, maxPlantHealth);
        PlantType plantType = PlantType.values()[generator.nextInt(PlantType.values().length)];
        double size = ThreadLocalRandom.current().nextDouble(minSize, maxSize);

       return new PlantSensorValue(lumen,soilMoisture,tempature,plantHealth,plantType,size);


    }


}
